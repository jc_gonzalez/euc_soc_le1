#!/bin/bash 
#------------------------------------------------------------------------------------
# LE1EnhanceServer.sh
# Launch LE1 Enhancement Server for remote requests of LE1 additional metadata
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2019-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $(which $0)); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}:${PYTHONATH}

# Launch LE1 Enhancement server
${PYTHON:=python3} ${SCRIPTPATH}/le1_enhance/srv_le1_enhance.py $* -p 8181 &


