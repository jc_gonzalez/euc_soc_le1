#!/bin/bash 
#------------------------------------------------------------------------------------
# le1_enhance.sh
# Enhance LE1 metadata
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2019-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $(which $0)); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}:${PYTHONATH}

#- Default values
CFGFILE=${SCRIPTPATH}/le1_enhance/le1_additional-params.json
ISFITS="no"
SERVER=""

TMPDIR=/tmp/$$.dir
OUTPUT_DIR=$(pwd)
LOGFILE=le1enh.log

usage () {
    echo "$0  [ -h ] -i <le1product> [ -o <le1outputname> ] [ -O <outputdir> ] [ -c <configfile> ] [ -s <server-URL> ] [ -l <logfile> ] [ -F ]"
    echo ""
    echo "where:"
    echo "  -h   Show this message"
    echo "  -i   Provides the LE1 input product to enhance"
    echo "  -o   Provides a new name for the output product"
    echo "  -O   Specifies the output directory to place the output product"
    echo "  -c   Specifies the config. file with the enhancement definitions"
    echo "  -s   Uses specified HTTP server URL (http://address:port) for metadata requests to LE1 Enhancement service"
    echo "  -l   Specifies the name of the log file"
    echo "  -F   Specifies that the input is not a 'product package' but just the LE1 FITS file"
    exit 1
}

## Parse command line
while getopts :hi:d:o:O:c:s:l:F OPT; do
    case $OPT in
        h|+h) usage ;;
        i|+i) INPUT_FILE="$OPTARG" ;;
        o|+o) OUTPUT_FILE="$OPTARG" ;;
        O|+O) OUTPUT_DIR="$OPTARG" ;;
        c|+c) CFGFILE="$OPTARG" ;;
        s|+s) SERVER="-s $OPTARG" ;;
        l|+l) LOGFILE="$OPTARG" ;;
        F|+F) ISFITS="yes" ;;
        *)    usage ; exit 2
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

if [ -z "$INPUT_FILE" ]; then
    echo "ERROR: No input file provided" > /dev/stderr
    exit 1
fi

# Get product to process
mkdir -p ${TMPDIR}/in ${TMPDIR}/out
CURDIR=$(pwd)

if [ "$ISFITS" = "no" ]; then
    cd ${TMPDIR}/in
    unzip ${INPUT_FILE}
    cd $CURDIR
    INPUT_FILE=$(echo ${TMPDIR}/in/*.fits)

    # Duplicate (for the time being) the metadata file
    cp ${TMPDIR}/in/*.xml ${TMPDIR}/out/

    PKGFILE=${TMPDIR}/$(basename ${OUTPUT_FILE} .fits).zip
fi

# Set output file name
if [ -n "$OUTPUT_FILE" ]; then
    OUTPUT_FILE=${OUTPUT_DIR}/$(basename $OUTPUT_FILE)
else
    OUTPUT_FILE=${OUTPUT_DIR}$(basename $INPUT_FILE)
fi

# Perform enhancement
${PYTHON:=python3} ${SCRIPTPATH}/le1_enhance/le1_enhance.py -c ${CFGFILE} \
               -f ${INPUT_FILE} -o ${TMPDIR}/out/ ${SERVER} 1>${LOGFILE} 2>&1
 
# Move output file to its place
if [ "$ISFITS" = "no" ]; then
    # Create output product package
    cd ${TMPDIR}/out
    zip ${PKGFILE} *
    cd ${CURDIR}
    mv ${PKGFILE} ${OUTPUT_DIR}
else
    mv ${TMPDIR}/out/$(basename $INPUT_FILE) ${OUTPUT_FILE}
fi

# Clean up
rm -rf ${TMPDIR}
