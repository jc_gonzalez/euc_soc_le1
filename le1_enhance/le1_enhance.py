#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""le1_enhance.py

From a configuration file, retrieve data for a given set of parameters
and enhance the metadata of the specified LE1 (VIS and NISP) products
according to the parameters and aggregation methods requested in that
file.

usage: le1_enhance.py [-h] [-c CONFIG_FILE] [-f INPUT_FILE] [-d INPUT_DIR]
                      [-o OUTPUT] [-g]

Test script to enhance LE1 products

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG_FILE, --config CONFIG_FILE
                        Configuration file to use (default: None)
  -f INPUT_FILE, --file INPUT_FILE
                        Input product (default: None)
  -d INPUT_DIR, --dir INPUT_DIR
                        Input files directory (default: None)
  -o OUTPUT, --output OUTPUT
                        Output file name (default: None)
  -g, --gui             Output file name (default: False)


Sample config. file (le1_enh.json):
{
    "header": {
        "title": "Level 1 - Additional parameters configuration",
        "description": "Parameters to be added for LE1 products",
        "version": "0.1"
    },
    "data": {
        "VIS": {
            "NDW00001": {
                "parameter": "NDW00001",
                "timespan": { "type": "observation" },
                "dataset": { "mode": "summary"
            },
            "NDW00138c": {
                "parameter": "NDW00138",
                "timespan": { "type": "modified", "begin": 21600, "end": -21600 },
                "dataset": { "mode": "linear" }
            }
        },
        "NIR": {},
        "SIR": {}
    }
}

Sample command line:
$ python3 ./le1_enhance.py -d ./in -o ./out -c ./le1_enh.json

"""
#-----------------------------------------------------------------------

# make print & unicode backwards compatible
from __future__ import print_function
from __future__ import unicode_literals

import os, sys
_filedir_ = os.path.dirname(__file__)
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

#from tkinter import *

PYTHON2 = False
PY_NAME = "python3"
STRING = str

#-----------------------------------------------------------------------

#from appgui import App
import argparse
import glob
import json

from le1_enhancer import LE1_FilesMetadataEnhancer

import logging
logger = logging.getLogger()

#-----------------------------------------------------------------------

VERSION = '0.0.2'

__author__ = "J C Gonzalez"
__copyright__ = "Copyright 2015-2019, J C Gonzalez"
__license__ = "LGPL 3.0"
__version__ = VERSION
__maintainer__ = "J C Gonzalez"
__email__ = "jcgonzalez@sciops.esa.int"
__status__ = "Development"
#__url__ = ""

#-----------------------------------------------------------------------

def configureLogs():
    logger.setLevel(logging.DEBUG)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers
    #c_format = logging.Formatter('%(asctime)s %(levelname).1s %(name)s %(module)s:%(lineno)d %(message)s',
    #                             datefmt='%y-%m-%d %H:%M:%S')
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    for lname in os.getenv('LOGGING_MODULES', '').split(':'):
        lgr = logging.getLogger(lname)
        if not lgr.handlers: lgr.addHandler(c_handler)

def get_args():
    """
    Parse arguments from command line

    :return: args structure
    """
    parser = argparse.ArgumentParser(description='Test script to enhance LE1 products',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-c', '--config', dest='config_file',
                        help='Configuration file to use')
    parser.add_argument('-f', '--file', dest='input_file',
                        help='Input product')
    parser.add_argument('-d', '--dir', dest='input_dir',
                        help='Input files directory')
    parser.add_argument('-o', '--output', dest='output', default=None,
                        help='Output file name')
    parser.add_argument('-s', '--server', dest='server', default=None,
                        help='Server to use, in case the data is to be obtained remotely')
    parser.add_argument('-g', '--gui', dest='gui', default=False, action='store_true',
                        help='Use the Graphical User Interface')
    return parser.parse_args()

def splitFileName(file):
    fileinit, file_ext = os.path.splitext(file)
    file_path = os.path.dirname(fileinit)
    file_bname = os.path.basename(fileinit)
    return file_path, file_bname + file_ext, file_bname, file_ext[1:]

def greetings(output_dir, file_list, server=None):
    """
    Says hello
    """
    logger.info('='*60)
    logger.info('le1_enhance - Add metadata to LE1 products from HMS parameters')
    logger.info('-'*60)
    logger.info('The output folder is: {}'.format(output_dir))
    logger.info('The following product enhancements will take place:')
    for f, fo, _ in file_list:
        logger.info('- {}'.format(f))
        logger.info('  => {}'.format(fo))
    logger.info('The original files will be renamed with an ".orig" suffix if needed')
    if not server is None:
        logger.info(f'Metadata will be retrieved remotely from the server: {server}')
    logger.info('-'*60)

def processArgs(args):
    """
    Process arguments from command line
    :return: File list and configuration
    """
    # Compose input files list
    input_files = []
    if args.input_file:
        input_files.append(os.path.realpath(args.input_file))
    if args.input_dir:
        [input_files.append(os.path.realpath(x))
                            for x in glob.glob(args.input_dir + '/*')]

    if not input_files:
        logger.critical('ERROR: No input files provided. Exiting.')
        sys.exit(1)

    # Check output file
    if args.output is None:
        # Assume output dir is current dir
        output_dir = os.getcwd()
    else:
        output_dir = args.output
        if not os.path.isdir(output_dir):
            logger.critical('ERROR: Output directory {} does not exist. Exiting.'.format(output_dir))
            sys.exit(1)

    # Check config file
    if not args.config_file:
        logger.critical('ERROR: No configuration for product enhancement provided. Exiting.')
        sys.exit(1)

    remoteRqst = args.server is not None

    # Build list of file names (input, output, and backup if needed)
    file_list = []
    for f in input_files:
        f_path, f_name, f_bname, f_ext = splitFileName(f)
        f_out = os.path.join(output_dir, f_name)
        f_orig = None
        if f == f_out:
            f_orig = os.path.join(output_dir, '{}.{}.{}'.format(f_bname, 'orig', f_ext))
        file_list.append([f, f_out, f_orig])

    input_files.clear()

    # Read configuration
    with open(args.config_file, 'r') as cfgFp:
        enh_config = json.load(cfgFp)

    return file_list, output_dir, enh_config, remoteRqst

def main():
    """
    Main program
    """
    configureLogs()

    args = get_args()

    if args.gui:
        #root = Tk()
        #app = App(parent=root)
        #root.mainloop()
        return

    file_list, output_dir, enh_config, remoteRqst = processArgs(args)

    # Say hello, and list process parameters
    greetings(output_dir, file_list)

    le1enh = LE1_FilesMetadataEnhancer(file_list, enh_config)
    if not remoteRqst:
        # Create main object, and launch process
        le1enh.process()
    else:
        # Make the request, the returned data should be the requsted info in JSON
        le1enh.performRequest(server=args.server)


if __name__ == '__main__':
    main()
