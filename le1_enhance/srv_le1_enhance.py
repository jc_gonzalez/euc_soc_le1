#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""le1_enhance.py

From a configuration file, retrieve data for a given set of parameters
and enhance the metadata of the specified LE1 (VIS and NISP) products
according to the parameters and aggregation methods requested in that
file.

usage: le1_enhance.py [-h] [-c CONFIG_FILE] [-f INPUT_FILE] [-d INPUT_DIR]
                      [-o OUTPUT] [-g]

Test script to enhance LE1 products

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG_FILE, --config CONFIG_FILE
                        Configuration file to use (default: None)
  -f INPUT_FILE, --file INPUT_FILE
                        Input product (default: None)
  -d INPUT_DIR, --dir INPUT_DIR
                        Input files directory (default: None)
  -o OUTPUT, --output OUTPUT
                        Output file name (default: None)
  -g, --gui             Output file name (default: False)


Sample config. file (le1_enh.json):
{
    "header": {
        "title": "Level 1 - Additional parameters configuration",
        "description": "Parameters to be added for LE1 products",
        "version": "0.1"
    },
    "input": {
        "filename": "MyFileName.fits",
        "start": '2018-07-17T00:00:00.000000',
        "end": '2018-07-17T00:10:00.000000'
    }
    "data": {
        "VIS": {
            "NDW00001": {
                "parameter": "NDW00001",
                "timespan": { "type": "observation" },
                "dataset": { "mode": "summary"
            },
            "NDW00138c": {
                "parameter": "NDW00138",
                "timespan": { "type": "modified", "begin": 21600, "end": -21600 },
                "dataset": { "mode": "linear" }
            }
        },
        "NIR": {},
        "SIR": {}
    }
}

Sample command line:
$ python3 ./le1_enhance.py -d ./in -o ./out -c ./le1_enh.json

"""
#-----------------------------------------------------------------------

# make print & unicode backwards compatible
from __future__ import print_function
from __future__ import unicode_literals

import os, sys
_filedir_ = os.path.dirname(__file__)
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

#from tkinter import *

PYTHON2 = False
PY_NAME = "python3"
STRING = str

#-----------------------------------------------------------------------

#from appgui import App
import argparse
import glob
import json
import pickle
from datetime import datetime

from http.server import BaseHTTPRequestHandler, HTTPServer

from le1_enhancer import LE1_FilesMetadataEnhancer

import logging
logger = logging.getLogger()

#-----------------------------------------------------------------------

VERSION = '0.0.2'

__author__ = "J C Gonzalez"
__copyright__ = "Copyright 2015-2019, J C Gonzalez"
__license__ = "LGPL 3.0"
__version__ = VERSION
__maintainer__ = "J C Gonzalez"
__email__ = "jcgonzalez@sciops.esa.int"
__status__ = "Development"
#__url__ = ""

#-----------------------------------------------------------------------

class LE1EnhancementServiceHandler(BaseHTTPRequestHandler):
    def setResponse(self, respCode=200, respType='text/html', content=''):
        self.send_response(respCode)
        self.send_header('Content-type', respType)
        self.end_headers()
        self.wfile.write(content)

    def do_GET(self):
        logger.info("GET request,\nPath: %s\nHeaders:\n%s\n", str(self.path), str(self.headers))
        self.setResponse(content='GET request for {}'.format(self.path).encode('utf-8'))

    def do_POST(self):
        """
        Handle a POST request
        If there is any problem getting the expected JSON config., an error is returned
        """
        # Get POSTed data
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself
        data = post_data.decode('utf-8')
        logger.info("POST request,\nPath: %s\nHeaders:\n%s\n\nBody:\n%s\n",
                    str(self.path), str(self.headers), data)

        # Try to decode JSON configuration
        try:
            self.enh_config = json.loads(data)
        except:
            msg = 'Posted data cannot be decode as LE1 Enh. config. file'
            logger.warning(msg)
            self.setResponse(respCode=500, content=msg.encode('utf-8'))
            return

        # Successful decoding, check configuration
        try:
            input_file = os.path.realpath(self.enh_config['input']['filename'])
        except:
            msg = 'LE1 Enh. config. incomplete (problem with input file)'
            logger.warning(msg)
            self.setResponse(respCode=500, content=msg.encode('utf-8'))
            return

        try:
            self.datetime_start = datetime.strptime(self.enh_config['input']['start'], '%Y-%m-%dT%H:%M:%S.%f')
            self.datetime_end = datetime.strptime(self.enh_config['input']['end'], '%Y-%m-%dT%H:%M:%S.%f')
        except:
            msg = 'Cannot decode start and/or end times in config. file'
            logger.warning(msg)
            self.setResponse(respCode=500, content=msg.encode('utf-8'))
            return

        le1enh = LE1_FilesMetadataEnhancer([input_file, '', ''], self.enh_config)
        result = le1enh.processSingleFileRequest(input_file,
                                                 self.datetime_start, self.datetime_end)

        self.setResponse(content=pickle.dumps(result, protocol=0))

def runServer(server_class=HTTPServer, handler_class=LE1EnhancementServiceHandler, port=8181):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logger.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logger.info('Stopping httpd...\n')

def splitFileName(file):
    fileinit, file_ext = os.path.splitext(file)
    file_path = os.path.dirname(fileinit)
    file_bname = os.path.basename(fileinit)
    return file_path, file_bname + file_ext, file_bname, file_ext[1:]

def configureLogs():
    logger.setLevel(logging.DEBUG)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(logging.DEBUG)

    # Create formatters and add it to handlers
    #c_format = logging.Formatter('%(asctime)s %(levelname).1s %(name)s %(module)s:%(lineno)d %(message)s',
    #                             datefmt='%y-%m-%d %H:%M:%S')
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    for lname in os.getenv('LOGGING_MODULES', '').split(':'):
        lgr = logging.getLogger(lname)
        if not lgr.handlers: lgr.addHandler(c_handler)

def get_args():
    """
    Parse arguments from command line

    :return: args structure
    """
    parser = argparse.ArgumentParser(description='Test script to enhance LE1 products',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p', '--port', dest='port', type=int, default=8181,
                        help='Port to use to get server connections')
    return parser.parse_args()

def greetings():
    """
    Says hello
    """
    logger.info('='*60)
    logger.info('srv_le1_enhance - LE1 Enhancement Metadata (from HMS parameters) Server')
    logger.info('-'*60)

def main():
    """
    Main program
    """
    configureLogs()

    args = get_args()

    # Say hello, and list process parameters
    greetings()
    runServer(port=args.port)


if __name__ == '__main__':
    main()
